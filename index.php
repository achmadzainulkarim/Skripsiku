<?php
    include "header.php";
?>

<script type="text/javascript" >
$(document).ready(function() {
    document.getElementById('home').setAttribute('class', 'active');
});
</script>
<html>
    <?php
    include "navbar.php";
    ?>
    <body>
        <div class="col-md-10" style="float: right;">
            <br><br><br>
            <hr>
            <div class="col-md-4">
                <a href="data-training.php" class="btn btn-block btn-primary">
                    <br>
                    <span class="fa fa-bar-chart-o" style="font-size:80px"></span><br>
                    <h3>Data Training</h3>
                </a>
            </div>
            <div class="col-md-4">
                <a href="data-testing.php" class="btn btn-block btn-primary">
                    <br>
                    <span class="fa fa-line-chart" style="font-size:80px;"></span><br>
                    <h3>Data Testing</h3>
                </a>
            </div>
            <div class="col-md-4">
                <a href="optimasi.php"  class="btn btn-block btn-primary">
                    <br>
                    <span class="fa fa-check-square-o" style="font-size:80px"></span><br>
                    <h3>Nilai K-Optimal</h3>
                </a>
            </div>
        </div>
    </body>
</html>