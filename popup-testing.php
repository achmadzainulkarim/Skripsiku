<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="modal_save">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="text-align:center;">Identifikasi Data Segmentasi</h4>
			</div>
			<div class="modal-body">
					<form action="hasil.php" name="modal_popup" enctype="multipart/form-data" method="POST">
						<div class="form-group" style="padding-bottom: 20px;">
							<label style="margin: auto;" class="col-md-3" >A</label>
							<input readonly type="text" name="a" class="form-control-static col-md-2" id="modal_hasil_a">
							<label style="margin: auto;" class="col-md-3" >B</label>
							<input readonly type="text" name="b" class="form-control-static col-md-2" id="modal_hasil_b">
							<label style="margin: auto;" class="col-md-3" >C</label>
							<input readonly type="text" name="c" class="form-control-static col-md-2" id="modal_hasil_c">
							<label style="margin: auto;" class="col-md-3" >D</label>
							<input readonly type="text" name="d" class="form-control-static col-md-2" id="modal_hasil_d">
							<label style="margin: auto;" class="col-md-3" >E</label>
							<input readonly type="text" name="e" class="form-control-static col-md-2" id="modal_hasil_e">
							<label style="margin: auto;" class="col-md-3" >F</label>
							<input readonly type="text" name="f" class="form-control-static col-md-2" id="modal_hasil_f">
							<label style="margin: auto;" class="col-md-3" >G</label>
							<input readonly type="text" name="g" class="form-control-static col-md-2" id="modal_hasil_g">
							<br><br><br><br>
							<br><br>
			</div>
			<hr>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<button type="submit" class="btn btn-primary" name="simpan"><span class="fa fa-search"></span> &nbsp;Identifikasi</button>
				<button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> &nbsp;Batal</button>
			</form>
			</div>
		</div>
	</div>
</div>
<!-- Javascript untuk popup modal Delete--> 
<script type="text/javascript">
function popup_modal_save()
{
	$('#modal_save').modal('show', {backdrop: 'static'});
	$('#modal_hasil_a').val($('#hasil_a').text());
	$('#modal_hasil_b').val($('#hasil_b').text());
	$('#modal_hasil_c').val($('#hasil_c').text());
	$('#modal_hasil_d').val($('#hasil_d').text());
	$('#modal_hasil_e').val($('#hasil_e').text());
	$('#modal_hasil_f').val($('#hasil_f').text());
	$('#modal_hasil_g').val($('#hasil_g').text());
}
</script>