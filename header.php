<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/navbar.css">
<link rel="stylesheet" href="assets/css/font-awesome.css">
<script src="assets/js/jquery.js" type="text/javascript"></script>
<script src="assets/js/render.js" type="text/javascript"></script>
<script src="assets/js/functions.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.js" type="text/javascript"></script>
<style>
    .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
        background-color: #2f0051 !important;
        color: #ffe100;

    }
    .btn-warning, .btn-warning:hover, .btn-warning:active, .btn-warning:visited {
        background-color: #ffe100 !important;
        color: #2f0051;
    }
</style>