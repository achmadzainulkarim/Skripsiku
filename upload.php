<html>
    <head>
        <title>
            Aplikasi Identifikasi Tanda Tangan
        </title>
        <link rel="stylesheet" href="assets/css/custom.css" type="text/css">
        <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
        <script src="assets/js/jquery.js" type="text/javascript"></script>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/render.js" type="text/javascript"></script>
        <script src="assets/js/functions.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <!-- FORM UNTUK UPLOAD GAMBAR KE DALAM HALAMAN -->
            <center>
                <img src="" id="preview" height="200">
                <br><br>
                 <label class="btn btn-success custom-file-upload ">
                    Upload Image
                    <input type="file" onchange="previewFile()" style="display : none">
                </label>
                <br>
                <!--Mengambil citra inputan            -->
                <div class="col-md-6 " >
                    <button class="btn btn-info" onclick="akuisisi()">
                        Akuisisi Citra
                    </button>
                </div>
                
                <div class="col-md-6">
                    <button class="btn btn-info" onclick="grayscale()">
                        Citra Grayscale
                    </button>
                </div>

                <div class="col-md-6">
                    <canvas id="imagebaru" height="200" width="200"></canvas>
                </div>

                <div class="col-md-6">
                    <canvas id="grayscale" height="200" width="200"></canvas>
                </div>

                <div class="col-md-6">
                    <button class="btn btn-info" onclick="binary()" id="button-binary">
                        Citra Binary
                    </button>
                </div>

                <div class="col-md-6">
                    <button class="btn btn-info" onclick="invert()" id="button-invert">
                        Citra Invert
                    </button>
                </div>

                <div class="col-md-6">
                    <canvas id="binary" height="200" width="200"></canvas>
                </div>

                <div class="col-md-6">
                    <canvas id="invert" height="200" width="200"></canvas>
                </div>
                
                
                <div class="col-md-6">
                    <button class="btn btn-info" onclick="autocrop()" id="button-auto-crop">
                        Citra Auto Cropping
                    </button>
                </div>

                <div class="col-md-6">
                    <canvas id="autocrop" height="200" width="200"></canvas>
                </div>
                
                
            </center>
        </div>
    </body>
</html>