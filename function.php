<?php
	function kuadrat($a){
		$hasil = $a * $a;
		return $hasil;
	}
	function distance($a, $b){
		$total = 0;
		$jumlah = count($a);
		for ($i=0; $i < $jumlah; $i++) { 
			$kurang = kuadrat($a[$i] - $b[$i]);
			$total = $total + $kurang;
		}
		$hasil = sqrt($total);
		return $hasil;
	}
?>