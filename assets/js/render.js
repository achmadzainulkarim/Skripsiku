function Citra(canvas,x,y,w,h){
	var kanvas=document.getElementById(canvas);
	kanvas.crossOrigin = 'anonymous';
	this.image=kanvas.getContext("2d");
	this.idata=this.image.getImageData(x,y,w,h);
	this.x=x;
	this.y=y;
	this.w=w;
	this.h=h;
	this.inRange=function(px,py){
		if(px>=0 && py>=0 && px<=this.w && py<this.h){
			return true;
		}else{
			return false;
		}
	}
	this.getPixel=function(px,py){
		var arr=Array();
		var i=((py*this.idata.width)+px)*4;
		arr[0]=this.idata.data[i+0];
		arr[1]=this.idata.data[i+1];
		arr[2]=this.idata.data[i+2];
		arr[3]=this.idata.data[i+3];
		return arr;
	}
	this.setPixel=function(px,py,arr){
		var i=((py*this.idata.width)+px)*4;
		this.idata.data[i+0]=arr[0];
		this.idata.data[i+1]=arr[1];
		this.idata.data[i+2]=arr[2];
		this.idata.data[i+3]=arr[3];
	}
	this.grayscale=function(){
		for(var y=0;y<this.h;y++){
			for(var x=0;x<this.w;x++){
				tmp=this.getPixel(x,y);
				var gs=((tmp[0]+tmp[1]+tmp[2])/3);
				tmp[0]=gs;
				tmp[1]=gs;
				tmp[2]=gs;
				this.setPixel(x,y,tmp);
			}
		}
	}
	this.binary=function(sp){
		if(sp==undefined)sp=128;
		for(var y=0;y<this.h;y++){
			for(var x=0;x<this.w;x++){
				tmp=this.getPixel(x,y);
				var gs=(((tmp[0]+tmp[1]+tmp[2])/3)<sp)?0:255;
				tmp[0]=gs;
				tmp[1]=gs;
				tmp[2]=gs;
				this.setPixel(x,y,tmp);
			}
		}
	}
	this.invert=function(){
		for(var y=0;y<this.h;y++){
			for(var x=0;x<this.w;x++){
				tmp=this.getPixel(x,y);
				tmp[0]=255-tmp[0];
				tmp[1]=255-tmp[1];
				tmp[2]=255-tmp[2];
				this.setPixel(x,y,tmp);
			}
		}
	}
   
	this.crop=function(){
		for(var y=0;y<this.h;y++){
			for(var x=0;x<this.w;x++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					var yt = y+1;
					break;
				}
			}
		}
		for(var y=this.h;y>0;y--){
			for(var x=this.w;x>0;x--){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					var yb = y+1;
					break;
				}
			}
		}
		for(var x=0;x<this.w;x++){
			for(var y=0;y<this.h;y++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					var xl = x+1;
					break;
				}
			}
		}
		for(var x=this.w;x>0;x--){
			for(var y=this.h;y>0;y--){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					var xr = x+1;
					break;
				}
			}
		}
		var sumber = document.getElementById('proses');
		var hsumber = sumber.height;
		var wsumber = sumber.width;
		var newh = yt - yb;
		var neww = xl - xr;
		// console.log("yt = "+yt+" yb = "+yb+" xl = "+xl+" xr = "+xr);
		// console.log("tinggi = "+newh+" lebar = "+neww);
		var akuisisi = document.getElementById('akuisisi');
		var context = akuisisi.getContext('2d');
		context.drawImage(akuisisi,xr,yb,neww, newh , 0, 0, wsumber, hsumber);
   	} 

  	this.segmentasi=function(){
  		var a = 0;
  		for(var y=0;y<300;y++){
			for(var x=0;x<=119;x++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					 a = a + 1;
					 // console.log(x+","+y);
				}
			}
		}
		// console.log("a = "+a)

		var b = 0;
  		for(var y=0;y<300;y++){
			for(var x=120;x<=239;x++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					 b = b + 1;
				}
			}
		}
		// console.log("b = "+b)
		

		var c = 0;
  		for(var y=0;y<300;y++){
			for(var x=240;x<=359;x++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					 c = c + 1;
				}
			}
		}
		// console.log("c = "+c)
		var d = 0;
  		for(var y=0;y<300;y++){
			for(var x=360;x<=479;x++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					 d = d + 1;
				}
			}
		}
		// console.log("d = "+d)
		var e = 0;
  		for(var y=0;y<300;y++){
			for(var x=480;x<=599;x++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					 e = e + 1;
				}
			}
		}
		// console.log("e = "+e)
		var f = 0;
  		for(var y=0;y<=149;y++){
			for(var x=0;x<600;x++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					 f = f + 1;
				}
			}
		}
		// console.log("f = "+f)
		var g = 0;
  		for(var y=150;y<=299;y++){
			for(var x=0;x<600;x++){
				tmp=this.getPixel(x,y);
				if (tmp[0]==0){
					 g = g + 1;
				}
			}
		}
		// console.log("g = "+g)
        $('#hasil_a').text(a);
        $('#hasil_b').text(b);
        $('#hasil_c').text(c);
        $('#hasil_d').text(d);
        $('#hasil_e').text(e);
        $('#hasil_f').text(f);
        $('#hasil_g').text(g);

  	}
	//getAvgRect(x,y,<rect size>);
	this.getAvgRect=function(x,y,s){
		var arr=Array(0,0,0,0);
		var fact=0;
		var c=Math.floor(s/2);
		for(var oy=0-c;oy<=c;oy++){
			for(var ox=0-c;ox<=c;ox++){
				var nx=x+ox;
				var ny=y+oy;
				if(this.inRange(nx,ny)){
					var tmp=this.getPixel(nx,ny);
					arr[0]=arr[0]+tmp[0];
					arr[1]=arr[1]+tmp[1];
					arr[2]=arr[2]+tmp[2];
					arr[3]=arr[3]+tmp[3];
					fact++;
				}
			}
		}
		arr[0]=arr[0]/fact;
		arr[1]=arr[1]/fact;
		arr[2]=arr[2]/fact;
		arr[3]=arr[3]/fact;
		return arr;
	}
	this.render=function(cv,x,y,dx,dy,dw,dh){
		if(dx==undefined)dx=0;
		if(dy==undefined)dy=0;
		if(dw==undefined)dw=this.idata.width;
		if(dh==undefined)dh=this.idata.height;
		var hasil2=document.getElementById(cv);
		if(hasil2.width==0 && hasil2.height==0){
			hasil2.width=dw;
			hasil2.height=dh;
		}
		cv=hasil2.getContext("2d");
		cv.putImageData(this.idata,x,y,dx,dy,dw,dh);
	}

}