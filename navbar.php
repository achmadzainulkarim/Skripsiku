<div class="navbar navbar-default navbar-fixed-left">
  <a class="navbar-brand" href="#"></a>
  <ul class="nav navbar-nav">
   <li id="home"><a href="." ><span class="fa fa-home" style="font-size:18px"></span> Home</a></li>
   <li id="data-training"><a href="data-training.php"><span class="fa fa-bar-chart-o" style="font-size:18px"></span> Data Training</a></li>
   <li id="data-testing"><a href="data-testing.php" ><span class="fa fa-line-chart" style="font-size:18px"></span> Data Testing</a></li>
   <li id="optimal"><a href="optimasi.php" ><span class="fa fa-check-square-o" style="font-size:18px"></span> &nbsp; Nilai K-Optimal</a></li>
  </ul>
</div>

	<!--Navbar-->
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
     <!--Logo Header-->
     <div class="navbar-header">
     	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainnavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
         </button>
         <a href="index.php" class="navbar-brand">Sistem Identifikasi Tanda Tangan</a>
     </div>

     <!--isi header kanan-->
     <div class="collapse navbar-collapse" id="mainnavbar">
     </div>
 </div>
</nav>