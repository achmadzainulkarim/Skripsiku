<?php
    include "header.php";
?>
<script type="text/javascript" >
$(document).ready(function() {
    document.getElementById('data-testing').setAttribute('class', 'active');
});
$('#file_input').click(function() {
  $('#title').val(this.files && this.files.length ? this.files[0].name.split('.')[0] : '');

})

function returning(){
	$('#preview').css('display','');
	$('#akuisisi').css('display','none');
}
function processing(){
	document.getElementById('btn_akuisisi').click();
	document.getElementById('btn_grayscale').click();
	document.getElementById('btn_biner').click();
	document.getElementById('btn_crop').click();
	document.getElementById('btn_segmentasi').click();
	document.getElementById('btn_identify').click();
}

</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<html>
	<?php
	include "navbar.php";
	include 'config.php';
	include 'function.php';
	?>
	<body>
		<div class="col-md-10" style="float: right;">
			<br><br><br>
			<h4>
				DATA TESTING
			</h4>
			<hr/>
			<div class="col-md-9">
				<center>
					<img id="preview" height="300px" width="600px">
					<canvas id="akuisisi" height="300px" width="600px" style="display:none;"></canvas>
					<canvas id="proses" height="300px" width="600px" style="display:none;"></canvas>
					<br><br>
					<label class="btn btn-warning custom-file-upload ">
						Upload Image
						<input type="file" onchange="previewFile()" style="display : none" id="file_input">
					</label>
				</center>
			<h4>
			HASIL SEGMENTASI
			</h4>
			<hr>
			<table class="table table-hover">
				<tr>
					<th>No</th>
					<th>Pola</th>
					<th>Jumlah Piksel Hitam</th>
				</tr>
				<tr>
					<td>1</td><td>A</td><td id="hasil_a"></td>
				</tr>
				<tr>
					<td>2</td><td>B</td><td id="hasil_b"></td>
				</tr>
				<tr>
					<td>3</td><td>C</td><td id="hasil_c"></td>
				</tr>
				<tr>
					<td>4</td><td>D</td><td id="hasil_d"></td>
				</tr>
				<tr>
					<td>5</td><td>E</td><td id="hasil_e"></td>
				</tr>
				<tr>
					<td>6</td><td>F</td><td id="hasil_f"></td>
				</tr>
				<tr>
					<td>7</td><td>G</td><td id="hasil_g"></td>
				</tr>
			</table>

			</div>
			<div class="col-md-3">    
				<div style="display:none">
					<button id="btn_akuisisi" class="btn btn-block btn-warning" onclick="akuisisi()">Akuisisi</button>
					<button id="btn_grayscale" class="btn btn-block btn-warning" onclick="grayscale()">Grayscale</button>
					<button id="btn_biner" class="btn btn-block btn-warning" onclick="binary()">Biner</button>
					<button id="btn_crop" class="btn btn-block btn-warning" onclick="autocrop()">Crop</button>
					<button id="btn_segmentasi" class="btn btn-block btn-warning" onclick="zona_segmentasi()">Segmentasi</button>
					<a href="#" id="btn_identify" class="btn btn-block btn-warning" onclick="popup_modal_save()">Identifikasi</a>
				</div>
				<button id="btn_proses" class="btn btn-block btn-warning" onclick="processing()">Proses</button>
				<h4>Properti Citra</h4>
				<label id="title">
				</label>
			</div>
		</div>

		<?php
			include "popup-testing.php";

?>
	</body>
</html>